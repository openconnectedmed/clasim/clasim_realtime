#include "Pump.h"

using namespace CLA;

/* wrapper for pump tcp client */
Pump::Pump(std::shared_ptr<CLA::LOGGER>& logger, const string& substance,
const string& topic, const string& type) : m_logger(logger), m_substance(substance),
m_tcp_pump_client(topic, type, substance, logger) {
    /* setup connection to server via a tcp socket */
    m_tcp_pump_client.setup_connection();
}

double Pump::getCurrentInfusionRate () {
  current_rate = m_tcp_pump_client.getCurrentInfusionRate(m_substance);
  return current_rate;
}

string Pump::getSubstance() const {
  return m_substance;
}

Pump::~Pump() {
}
