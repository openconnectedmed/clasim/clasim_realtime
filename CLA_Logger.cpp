#include "CLA_Logger.h"


CLA::LOGGER::LOGGER(const string& name): m_patient_name(move(name)){
    /* timestamped logs */
    m_logstream[0].open(prosim_logs_dir+"prosim_"+m_patient_name+current_time(true)+".log", \
                                                                    ios::out|ios::binary);
    m_logstream[1].open(prosim_results_dir+"duration_"+m_patient_name+current_time(true)+".txt", \
                                                                    ios::out|ios::binary);
    m_logstream[2].open(prosim_results_dir+"duration_sim_monitor_"\
                        +m_patient_name+current_time(true)+".txt", \
                                            ios::out|ios::binary);
    m_logstream[3].open(prosim_results_dir+"data_"+m_patient_name+current_time(true)+".txt",\
                                                                    ios::out|ios::binary);
    m_logstream[0]<< "Date"<<setw(12)<<"Time"<<setw(12)<<"Tz"<<setw(3)<<" "\
                  <<"Level"<<setw(9)<<"Message"<<endl;
    m_logstream[1]<<"start time,"<<"engine adv duration(s),"<<"command duration(s),"\
                  <<"infusion duration(s),"<<"totals duration(s),"<<"next_update time"<<endl;
    m_logstream[2]<<"start time,"<<"engine adv duration(s),"<<"infusion duration(s),"\
                  <<"totals duration(s),"<<"next_update time"<<endl;
    m_logstream[3]<<" Engine Time,"<<"bpdrug rate,"<<"saline rate"<<endl;
    /* mapping strings to levels */
    logging_levels["DEBUG"] = CLA::LOGGER::DEBUG;
    logging_levels["INFO"]  = CLA::LOGGER::INFO;
    logging_levels["WARN"]  = CLA::LOGGER::WARN;
    logging_levels["ERROR"] = CLA::LOGGER::ERROR;
    logging_levels["FATAL"] = CLA::LOGGER::FATAL;
    logging_levels["NONE"]  = CLA::LOGGER::NONE;
}

CLA::LOGGER::LOGGER(const string& filename, const string& name): m_filename(move(filename)),
m_patient_name(move(name)) {
    m_logstream[0].open(prosim_logs_dir+m_filename+".log", ios::out|ios::binary);
    m_logstream[1].open(prosim_results_dir+"duration_"+m_filename+".txt", ios::out|ios::binary);
    m_logstream[2].open(prosim_results_dir+"duration_sim_monitor_"+\
                        m_filename+".txt", ios::out|ios::binary);
    m_logstream[3].open(prosim_results_dir+"data_"+m_filename+".txt", ios::out|ios::binary);
    m_logstream[0] << "Date"<<setw(12)<<"Time"<<setw(12)<<"Tz"<<setw(3)<<" "\
                   <<"Level"<<setw(9)<<"Message"<<endl;
    m_logstream[1]<<"start time,"<<"engine adv duration(s),"<<"command duration(s),"\
                  <<"infusion duration(s),"<<"totals duration(s),"<<"next_update time"<<endl;
    m_logstream[2]<<"start time,"<<"engine adv duration(s),"<<"infusion duration(s),"\
                  <<"totals duration(s),"<<"next_update time"<<endl;
    m_logstream[3]<<"Engine Time,"<<"bpdrug rate,"<<"saline rate"<<endl;
    /* mapping strings to levels */
    logging_levels["DEBUG"] = CLA::LOGGER::DEBUG;
    logging_levels["INFO"]  = CLA::LOGGER::INFO;
    logging_levels["WARN"]  = CLA::LOGGER::WARN;
    logging_levels["ERROR"] = CLA::LOGGER::ERROR;
    logging_levels["FATAL"] = CLA::LOGGER::FATAL;
    logging_levels["NONE"]  = CLA::LOGGER::NONE;
}

string CLA::LOGGER::current_time(bool display_timezone) {
    struct timeval tv; /* for seconds and micro seconds */
    gettimeofday(&tv , NULL); /* populate `tv` with time since epoch */
    char buffer[50];
    char m_sec_buff[10];
    char tz_buff[10];
    /* this is where the milli seconds come from :) */
    uint m_secs = static_cast<uint>(tv.tv_usec)/1000;
    sprintf(m_sec_buff, "%.3u", m_secs); /*cast unit to string */

    if (display_timezone) {
      strftime(buffer, 50, "%Y-%m-%d-%H %M %S,", localtime(&tv.tv_sec));
      strftime(tz_buff, 10, "  %Z", localtime(&tv.tv_sec));
      strcat(buffer, m_sec_buff);
      strcat(buffer, tz_buff);
    }
    else {
      strftime(buffer, 50, "%Y-%m-%d-%H %M %S", localtime(&tv.tv_sec));
    }
    cout << "time for files" << buffer <<endl;
    return buffer;
}

string CLA::LOGGER::format_duration(time_t t) {
  char buff[10];
  sprintf(buff, "%.3f", static_cast<float>(t)/1000);
  return buff;
}

string CLA::LOGGER::format_chrono_time(time_t t) {
    char buff[20];
    strftime(buff, 20, "%X", localtime(&t));
    return buff;
}

string CLA::LOGGER::format_chrono_time(system_clock::time_point t) {
    char buff[20];
    auto in_time = system_clock::to_time_t(t);
    strftime(buff, 20, "%X", localtime(&in_time));
    return buff;
}

string CLA::LOGGER::format_chrono_time_ms(system_clock::time_point t) {
    char buff[20];
    char m_sec_buff[10];
    auto in_time = system_clock::to_time_t(t);
    auto milli_secs = duration_cast<milliseconds>(t.time_since_epoch());
    uint m_secs = milli_secs.count()%1000;
    strftime(buff, 20, "%X.", localtime(&in_time));
    sprintf(m_sec_buff, "%d", m_secs);
    strcat(buff,m_sec_buff);
    return buff;
}

void CLA::LOGGER::log_duration(system_clock::time_point start_time, \
time_t engine_duration, time_t command_duration, time_t infusion_duration, \
time_t total_duration, time_t next_update_time) {
    ss<< format_chrono_time(start_time);
    ss<< ",";
    ss<< format_duration(engine_duration);
    ss<< ",";
    ss<< format_duration(command_duration);
    ss<< ",";
    ss<<format_duration(infusion_duration);
    ss<< ",";
    ss<<format_duration(total_duration);
    ss<< ",";
    ss<<format_chrono_time(next_update_time);
    m_logstream[1] << ss.str() << endl;
    ss.str("");
}

void CLA::LOGGER::log_duration(system_clock::time_point start_time, \
time_t engine_duration, time_t infusion_duration,time_t total_duration, \
time_t next_update_time) {
    ss<< format_chrono_time(start_time);
    ss<< ",";
    ss<< format_duration(engine_duration);
    ss<< ",";
    ss<<format_duration(infusion_duration);
    ss<< ",";
    ss<<format_duration(total_duration);
    ss<< ",";
    ss<<format_chrono_time(next_update_time);
    m_logstream[2] << ss.str() << endl;
    ss.str("");
}

void CLA::LOGGER::log_data(double engine_time, double bpdrug_rate, double \
  saline_rate) {
    ss<< engine_time;
    ss<< ",";
    ss<<bpdrug_rate;
    ss<<",";
    ss<<saline_rate;
    m_logstream[3] << ss.str() << endl;
    ss.str("");
}

void CLA::LOGGER::setLevel(const string& level) {
    /* all `logging levels` less than `level` in severity will be ignored*/
    DEFAULT_LEVEL = logging_levels[level];
}

void CLA::LOGGER::setVerbosity(bool verbosity) {
    /* logging details will be be printed streamed to stdout as
     *  well if `m_verbosity` is set to `true`, else otherwise
     */
    m_verbosity = verbosity;
}

string CLA::LOGGER::convert_level_to_string(LEVEL level) {
    switch(level) {
         case DEBUG:
             level_name = "DEBUG";
             break;
         case INFO:
             level_name = "INFO";
             break;
         case WARN:
             level_name = "WARN";
             break;
         case ERROR:
             level_name = "ERROR";
             break;
         case FATAL:
             level_name = "FATAL";
             break;
    }
    return level_name;
}

CLA::LOGGER::~LOGGER() {
    for (int i = 0; i < 4; i++) {
      m_logstream[i].flush();
      m_logstream[i].close();
    }
    ss.clear();
}
