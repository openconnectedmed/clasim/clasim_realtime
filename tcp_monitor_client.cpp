#include "tcp_monitor_client.h"

using namespace CLA;

tcp_monitor_client::tcp_monitor_client(std::shared_ptr<CLA::LOGGER>& logger,
const string& topic, const string& type): m_topic(move(topic)), m_type(move(type)),
m_logger(logger) {
    /* we can only talk to rosbrige server using json strings; here we want to
     * `publish` to `monitor topic` -> messages of type `std_msgs/Float64`
     */
    json_message = "{\"op\":\"advertise\", \"topic\": \""+m_topic+"\", \"type\": \""+m_type+"\"}";
    m_host = "localhost"; /* rosbridge server hostname */
    port = 9090; /* rosbridge server uses this port number */
    SOCKET_TIMEOUT_SEC = 0;
    SOCKET_TIMEOUT_uSEC = 500000;/* block for 0.5s */
}

void tcp_monitor_client::setup_connection() {
  /* create a TCP socket */
  fd = socket(AF_INET, SOCK_STREAM, 0);
  m_logger->debug("Trying to connect to dummy monitor through rosbridge server");
  if (fd < 0) {
    m_logger->error("Failed to create socket for publishing monitor data");
    exit(EXIT_FAILURE);
  }
  /* look up the address of the server given its name */
  hp = gethostbyname(m_host);
  if(!hp) {
    m_logger->error("Could not obtiain address");
    exit(EXIT_FAILURE);
  }
  /* initialize sockaddr_in servaddr*/
  memset((char*)&servaddr, 0, sizeof(servaddr));
  /* connect to remote host (rosbridge_server in our case)*/
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port); /* convert to network byte order */
  /* put host's address into th server address struct */
  memcpy((void*)&servaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
  addrlen = sizeof(servaddr);
  /*connect to server */
  if (connect(fd, (struct sockaddr*)&servaddr, addrlen) < 0) {
     m_logger->error("Connection to monitor Failed");
     exit(EXIT_FAILURE);
  }
  m_logger->info("Connection to monitor established established");
  /* send json message to server */
  int n = write(fd, json_message.c_str(), strlen(json_message.c_str()));
  if (n < 0) {
      m_logger->error("Can not write monitor data to socket");
  }
  m_logger->debug("Advertise request to "+m_topic+" sent to rosbridge server");
}

void tcp_monitor_client::publish_patient_data(const string& p_data) {
  timeout.tv_sec = SOCKET_TIMEOUT_SEC;
  timeout.tv_usec = SOCKET_TIMEOUT_uSEC;
  FD_ZERO(&set); /* initialize set */
  FD_SET(fd, &set);/* add descriptor to the write set */
  rec_value = select(FD_SETSIZE, NULL, &set, NULL, &timeout);
  if (rec_value == -1 ) {
    m_logger->error("socket error while connecting to "+m_topic);
    exit(EXIT_FAILURE);
  }
  else if (rec_value == 0) {
    /* on timeout -> keep calling the waiting_to_send function
     */
    waiting_to_send();
  }
  else {
    m_logger->info("publishing IBP: " + p_data);
    send_data_to_server(p_data);
  }
}

void tcp_monitor_client::send_data_to_server(const string& p_data) {
  /* rapidjson storing reference to string */
  m_patient_data = StringRef(p_data.c_str(), strlen(p_data.c_str()));
  _topic = StringRef(m_topic.c_str(), strlen(m_topic.c_str()));
  document.SetObject();
  Document::AllocatorType& allocator = document.GetAllocator();
  Value object(kObjectType);
  document.AddMember("op", "publish", allocator);
  document.AddMember("topic", _topic, allocator);
  object.AddMember("data", m_patient_data, allocator);
  document.AddMember("msg", object, allocator);

  Writer<StringBuffer> writer(strbuf);
  document.Accept(writer);
  string json_msg = strbuf.GetString();

  int i = send(fd, json_msg.c_str(), strlen(json_msg.c_str()), 0);
  if (i < 0) {
      m_logger->error("Can not write to socket to publish "+m_topic+" data");
  }
  strbuf.Clear();
  m_logger->debug("Publishing patient data to "+m_topic+" topic " );
}

void tcp_monitor_client::waiting_to_send() {
    m_logger->info("Waiting for new values to send ...");
}

void tcp_monitor_client::clear() {
  hp = nullptr;
}

tcp_monitor_client::~tcp_monitor_client() {
  m_logger->debug("Shutting down monitor socket connection");
  close(fd);
  clear();
  delete hp;
}
