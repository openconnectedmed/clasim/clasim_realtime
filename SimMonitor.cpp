#include "SimMonitor.h"

SimMonitor::SimMonitor( std::shared_ptr<CLA::LOGGER>& logger, \
CLA::Environment& env, std::map<std::string, CLA::Pump*>& pumps) : \
m_logger(logger), m_env(env), m_pumps(pumps), PatientOut(logger, env, pumps) {
    m_monitor = new CLA::Monitor(logger, "/dummy_monitor", "std_msgs/String");
    m_logger->info("Patient data will be published to /dummy_monitor topic");
}

void SimMonitor::update(std::unique_ptr<PhysiologyEngine>& engine) {
    /*
     * synchronize pulse with real-time clock
     */
    m_start = system_clock::now(); // start
    engine->AdvanceModelTime(advance_time, TimeUnit::s);
    m_engine_advance_end = system_clock::now();

    m_command_start = system_clock::now();
    systolic_pressure = engine->GetCardiovascularSystem()->GetSystolicArterialPressure(PressureUnit::mmHg);
    diastolic_pressure = engine->GetCardiovascularSystem()->GetDiastolicArterialPressure(PressureUnit::mmHg);
    heart_rate = engine->GetCardiovascularSystem()->GetHeartRate(FrequencyUnit::Per_min);
    resp_rate = engine->GetRespiratorySystem()->GetRespirationRate(FrequencyUnit::Per_min);
    oxygen_sat = engine->GetBloodChemistrySystem()->GetOxygenSaturation();

    /* publish data to dummy monitor topic for algorithm to capture
     * i.e. systolic/diastolic
     */
    m_monitor->publish_patient_data(to_string(systolic_pressure)+"/" \
                                    +to_string(diastolic_pressure));
    m_command_end = system_clock::now();

    /* perform infusions whenever the pump rate changes */
    m_infusion_start = system_clock::now();
    m_action.infuse_drug(engine);
    m_infusion_end = system_clock::now();// end

    /* start time for the next execution */
    m_next_update_time = system_clock::to_time_t(m_start) + m_next_start_in.count();
    /* compute durations */
    m_engine_duration_ms = duration_cast<milliseconds>(m_engine_advance_end -\
                           m_start).count();
    m_command_duration_ms = duration_cast<milliseconds>(m_command_end -\
                            m_command_start).count();
    m_infusion_duration_ms = duration_cast<milliseconds>(m_infusion_end -\
                             m_infusion_start).count();
    m_next_update_time_duration_ms = duration_cast<milliseconds>(m_next_start_in).count();
    /* total duration */
    m_total_duration_ms = duration_cast<milliseconds>(m_infusion_end - m_start).count();
    /* log data */
    m_logger->log_data(engine->GetSimulationTime(TimeUnit::s), \
                       m_action.get_bpdrug_rate(), m_action.get_saline_rate());
    /* log durations */
    m_logger->log_duration(m_start,m_engine_duration_ms, m_infusion_duration_ms,\
                           m_total_duration_ms, m_next_update_time);
    /* checking if simulation will take longer than allotted 'time' */
    if (m_total_duration_ms > m_next_update_time_duration_ms)
        m_logger->error("Simulation too slow");

    /* wait out the remaining time before starting the next execution */
    while(true) {
        m_delta = m_next_update_time - system_clock::to_time_t(system_clock::now());
        if (m_delta <= 0)
          break;
        std::this_thread::sleep_for(milliseconds(m_delta));
    }
    /* track data */
    engine->GetEngineTracker()->TrackData(engine->GetSimulationTime(TimeUnit::s));
}

void SimMonitor::Clear() {
    m_monitor = nullptr;
    m_pumps.clear();
}

SimMonitor::~SimMonitor() {
    Clear();
    delete m_monitor;
}
