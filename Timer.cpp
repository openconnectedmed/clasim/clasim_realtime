#include "Timer.h"

using namespace CLA;

Timer::Timer() {
    start_timer = steady_clock::now();
}

Timer::~Timer() {
    end_timer = steady_clock::now();
    duration = duration_cast<milliseconds>(end_timer-start_timer).count();
    std::cout<<"The simulation ran for "<< ((double)duration)/1000 <<"s\n";
}
