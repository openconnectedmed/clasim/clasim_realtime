# CLAsim - Real-Time Framework
The closed-loop assistant (CLA) simulation framework (CLAsim) enables simulation of the interaction between closed-loop medical systems consisting of patient monitors, closed-loop physiology management algorithms, and infusion pumps, and patient physiology. It is written in C++ and relies on the <a href="https://physiology.kitware.com/" target="_blank">Pulse Physiology Engine</a> for simulating patient physiology and the Robot Operating System (ROS) .

The framework comes in two flavors:

1. The software only framework, which provides a pure software-only, non-realtime, simulation of the patient interacting with devices and algorithms: https://gitlab.com/openconnectedmed/clasim/clasim_software_only
2. The real-time framework, which provides a real-time simulation of the patient, with interfaces to connect the patient to either real or simulated devices (this repository)

The current version is based on a demo example that we created for <a href="https://digitalcommons.bucknell.edu/masters_theses/220/" target="_blank">Farooq Gessa's masters thesis work</a>. It is a fork from <a href="https://gitlab.bucknell.edu/fmg005/cla_prosim_driver/tree/4a1575ea4453fe9bb88db3494cc84ced752cb42b" target="_blank">Farooq's repository</a>. We are working to make the framework easier to work with in terms of exploring other options beyond that demo scenario.

All files are licensed under the Apache 2.0 licence (see LICENSE file).

Copyright infromation is in the COPYRIGHT file.

If you have any questions, email <a href="mailto:info@openconnectedmed.org" target="_blank">info@openconnectedmed.org</a>

## Getting Started

### Installing CLAsim
CLAsim is has only been tested on Ubuntu 16.04. This is the recommended platform. We hope to support other platforms soon.

#### Pulse
CLAsim works with Pulse 2.1 and above. If you have this already installed, you can skip this section.

To install, go to the <a href="https://gitlab.kitware.com/physiology/engine" target="_blank">Pulse repository</a> and follow the instructions there. Be sure to follow the instructions for Ubuntu.

If you need to install or update cmake, be sure to use the install from source option.

 *For Java, be sure to use the openjdk version.*

 #### ROS
 CLAsim works with ROS Kinetic Kame. It also uses ROSBridge to communicate with nodes in ROS.

 1. Install ROS Kinetc Kame by following the instructions here: http://wiki.ros.org/kinetic/Installation
 2. Install ROSBridge
    ~~~bash
    # base on instructions here: http://wiki.ros.org/rosbridge_suite
    $ sudo apt-get install ros-kinetic-rosbridge-server
    ~~~


#### CLAsim Real-Time Framework
1. Clone the CLAsim repository into a folder of your choice. This will create a subfolder with the repository called `clasim_realtime` We will refer to the path to this  subfolder below as `<PathToCLAsimRT>`.
2. Compile CLAsim
   ~~~bash
   # make sure you are in the CLAsim folder
   $ cd <PathToCLAsimRT>
   # create a builds directory
   $ mkdir builds
   # enter builds folder
   $ cd builds
   # connect Pulse and CLASim for CMake Install
   # <PathToPulse> is the parent folder of the one you cloned Pulse into
   # that folder should contain the folders build and engine
   $ cmake .. -DCMAKE_PREFIX_PATH=<PathToPulse>/builds/install/
   # create CLASim and add executable to Pulse
   $ make
   $ make install
   ~~~
6. Test that the CLASim executable installed properly.
   ~~~bash
   # go to Pulse bin directory
   # (CLAsim executable installs there)
   $ cd <PathToPulse>/builds/install/bin
   # run CLAsim (if it runs and prints out something, then it installed properly)
   $ ./clasim_rt
   ~~~

### Running Demo Example
The CLAsim installation only has the patient model running with no devices connected to the patient and no algorithm managing the patient's physiology.

We have provided a demo physiology algorithm which can be found here: https://gitlab.com/openconnectedmed/clasim/clasim_realtime_algorithm_demo. You can follow the instructions there to run CLAsim with the algorithm. 
