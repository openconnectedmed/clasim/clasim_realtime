#include "tcp_pump_client.h"

using namespace CLA;

tcp_pump_client::tcp_pump_client(const string& topic, const string& type, \
  const string& substance, std::shared_ptr<CLA::LOGGER>& logger): \
  m_topic(move(topic)), m_type(move(type)), m_logger(logger), \
  m_substance(substance) {
    /* we can only talk to rosbrige server using json strings; here we want to
     * 'subscribe' to 'chatter topic' publishing messages of type 'std_msgs/Float64'
     */
    json_message = "{\"op\":\"subscribe\", \"topic\": \""+m_topic+"\", \"type\": \""+m_type+"\"}";
    m_host = "localhost"; /* rosbridge server runs hostname */
    port = 9090; /* rosbridge server uses this port number */
    SOCKET_TIMEOUT_SEC = 0;
    SOCKET_TIMEOUT_uSEC = 100000; /* 100 millisecond*/
    m_substance_last_sent_rate = 0;
    m_last_sent_rate = 0;
    m_new_rate = 0;
    m_current_rate = 0;
}

void tcp_pump_client::setup_connection() {
  /* create a TCP socket */
  fd = socket(AF_INET, SOCK_STREAM, 0);
    m_logger->debug("Trying to connect to "+m_substance+" pump through rosbridge server");

  if (fd < 0) {
    m_logger->error("Failed to create socket for "+m_substance+" pump");
    exit(EXIT_FAILURE);
  }
  /* look up the address of the server given its name */
  hp = gethostbyname(m_host);
  if(!hp) {
    m_logger->error("could not obtain address for "+m_substance+" pump");
    exit(EXIT_FAILURE);
  }
  /* initialize sockaddr_in servaddr*/
  memset((char*)&servaddr, 0, sizeof(servaddr));
  /* connect to remote host (rosbridge_server in our case)*/
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port); /* convert to network byte order */
  /* put host's address into th server address struct */
  memcpy((void*)&servaddr.sin_addr, hp->h_addr_list[0], hp->h_length);
  addrlen = sizeof(servaddr);
  /* initialize buffer s*/
  memset(buff, 0 , sizeof(buff));
  /*connect to server */
  if (connect(fd, (struct sockaddr*)&servaddr, addrlen) < 0) {
    m_logger->debug("Connection to "+m_substance+" pump failed");
    exit(EXIT_FAILURE);
  }
  m_logger->info("Connection to "+m_substance+" pump established");
  /* send json message to server */
  int n = write(fd, json_message.c_str(), strlen(json_message.c_str()));
  if (n < 0) {
      m_logger->error("Can not write to "+m_substance+" pump socket");
  }
  m_logger->debug("subscription to "+m_topic+" request sent to rosbridge server");
}

double tcp_pump_client::getCurrentInfusionRate(const string& substance_name) {
  timeout.tv_sec = SOCKET_TIMEOUT_SEC;
  timeout.tv_usec = SOCKET_TIMEOUT_uSEC;
  FD_ZERO(&set); /* initialize set */
  FD_SET(fd, &set);/* add descriptor to the read set */
  rec_value = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
  if (rec_value == -1 ) {
    m_logger->error(m_substance+" pump connection socket error");
    exit(EXIT_FAILURE);
  }
  else if (rec_value == 0) {
    /* on timeout -> keep calling the waiting_for_new_rate function
     * to return the last sent value until topic publishes a new rate
     */
    m_current_rate = waiting_for_new_rate(substance_name);
  }
  else {
    m_current_rate = get_new_rate(substance_name);
  }
  return m_current_rate;
}

double tcp_pump_client::get_new_rate(const string& substance_name) {
  recvlen = read(fd, buff, BUFF_SIZE);
  if (recvlen > 0) {
    buff[recvlen]  = '\0'; /* adding EOL */
    document.Parse(buff);
    if (substance_name == m_substance) {
      m_new_rate = document["msg"]["data"].GetDouble();
      m_substance_last_sent_rate = m_new_rate;
      if (m_new_rate)
        m_logger->info("New infusion rate for "+m_substance+" received: "\
        +to_string(int(m_new_rate))+" mL/hr");
    }
  }
  else {
    m_logger->error("Socket connection for "+m_substance+" data to rosbridge \
    server lost");
    exit(EXIT_FAILURE);
  }

  return m_new_rate;
}

double tcp_pump_client::waiting_for_new_rate(const string& substance_name) {
  /* while topic is not publishing, keep
   * returning the last sent rate value
   */
  if (substance_name == m_substance) {
    m_logger->debug(m_substance+": waiting for new infusion rate ...");
    m_last_sent_rate = m_substance_last_sent_rate;
  }
  return m_last_sent_rate;
}

void tcp_pump_client::clear() {
  hp = nullptr;
}

tcp_pump_client::~tcp_pump_client() {
  m_logger->debug("Shutting down "+m_substance+" pump connection");
  close(fd);
  clear();
  delete hp;
}
