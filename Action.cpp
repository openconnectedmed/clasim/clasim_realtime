#include "Action.h"

using namespace CLA;

Action::Action(std::shared_ptr<CLA::LOGGER>& logger, \
std::map<std::string,CLA::Pump*>& pumps) : m_logger(logger), m_pumps(pumps) {
  NorepiConcentration = 16;
  bpdrug_prev_rate = 0;
  saline_prev_rate = 0;
  SalineBagVolume = 500;
  bpdrug_rate = 0;
  saline_rate = 0;
}

void Action::initialize(std::unique_ptr<PhysiologyEngine>& engine) {
  if (!m_pumps["bpdrug"]->getSubstance().empty()) {
    m_bpdrug = engine->GetSubstanceManager().GetSubstance(m_pumps["bpdrug"]->getSubstance());
    m_bpdrug_infusion = new SESubstanceInfusion(*m_bpdrug);
    m_bpdrug_infusion->GetConcentration().SetValue(NorepiConcentration, MassPerVolumeUnit::ug_Per_mL);
  }
  if (!m_pumps["saline"]->getSubstance().empty()) {
    m_saline = engine->GetSubstanceManager().GetCompound(m_pumps["saline"]->getSubstance());
    m_infusion = new SESubstanceCompoundInfusion(*m_saline);
    m_infusion->GetBagVolume().SetValue(SalineBagVolume, VolumeUnit::mL);
  }
}

void Action::infuse_drug(std::unique_ptr<PhysiologyEngine>& engine) {
  bpdrug_rate = m_pumps["bpdrug"]->getCurrentInfusionRate();
  saline_rate = m_pumps["saline"]->getCurrentInfusionRate();

  if (!m_pumps["bpdrug"]->getSubstance().empty()) {
    if (bpdrug_prev_rate != bpdrug_rate && bpdrug_rate > 0) {
      m_bpdrug_infusion->GetRate().SetValue(bpdrug_rate, VolumePerTimeUnit::mL_Per_hr);
      engine->ProcessAction(*m_bpdrug_infusion);
      bpdrug_prev_rate = bpdrug_rate;
      m_logger->info(m_pumps["bpdrug"]->getSubstance()+": infusing patient at "\
      +to_string(int(bpdrug_rate))+" mL/hr");
    }
  }
  if (!m_pumps["saline"]->getSubstance().empty()) {
    if (saline_prev_rate != saline_rate && saline_rate > 0) {
      m_infusion->GetRate().SetValue(saline_rate, VolumePerTimeUnit::mL_Per_hr);
      engine->ProcessAction(*m_infusion);
      saline_prev_rate = saline_rate;
      m_logger->info(m_pumps["saline"]->getSubstance()+": infusing patient at "\
      +to_string(int(saline_rate))+" mL/hr");
    }
  }
}

double Action::get_bpdrug_rate() const {
  return bpdrug_rate;
}

double Action::get_saline_rate() const {
  return saline_rate;
}

void Action::clear() {
  m_bpdrug = nullptr;
  m_bpdrug_infusion = nullptr;
  m_infusion = nullptr;
  m_saline = nullptr;

}

Action::~Action() {
  clear();
  delete m_bpdrug;
  delete m_bpdrug_infusion;
  delete m_infusion;
  delete m_saline;
}
