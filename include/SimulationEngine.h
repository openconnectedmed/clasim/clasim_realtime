
#ifndef SIMENGINE_H
#define SIMENGINE_H

void Simulation(const std::string, const std::shared_ptr<Config>&);
void Global_LoadConfig( std::shared_ptr<CLA::LOGGER>&, std::unique_ptr<PhysiologyEngine>&,
const std::shared_ptr<Config>&, CLA::Environment&);


#endif
