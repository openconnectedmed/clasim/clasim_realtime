#ifndef Control_H_
#define Control_H_

#include "tcp_control_client.h"

namespace CLA {
  class Control {
    /* wrapper for control tcp client */
    private:
      CLA::tcp_control_client m_tcp_control_client;
      std::shared_ptr<CLA::LOGGER> m_logger;
    public:
      Control( std::shared_ptr<CLA::LOGGER>&);
      Control( std::shared_ptr<CLA::LOGGER>&, const string&, const string&);
      ~Control();
      void publish_control_message(const string&);
  };
}

#endif
