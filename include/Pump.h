#ifndef PUMP_H
#define PUMP_H

#include <string.h>
#include <iostream>
#include "tcp_pump_client.h"

using namespace std;

namespace CLA {
  class Pump {
    /* wrapper for tcp client */
    private:
      CLA::tcp_pump_client m_tcp_pump_client;
      string m_substance;
      double current_rate;
      std::shared_ptr<CLA::LOGGER> m_logger;
    public:
      Pump( std::shared_ptr<CLA::LOGGER>&, const string&, const string&, const string&);
      ~Pump();
      string getSubstance() const ;
      double getCurrentInfusionRate();
  };
}

#endif
