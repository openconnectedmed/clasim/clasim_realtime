#ifndef SimMonitor_H
#define SimMonitor_H

#include <string>
#include <iostream>
#include <cstdio>
#include <chrono>
#include <thread>
#include <map>
#include <libconfig.h++>
#include "CLA_Logger.h"
#include "Environment.h"
#include "Action.h"
#include "Pump.h"
#include "Monitor.h"
#include "PatientOut.h"


/* Pulse header files */
#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"


#include "engine/SEDataRequestManager.h"
#include "engine/SEEngineTracker.h"
#include "engine/SEEventManager.h"
#include "compartment/SECompartmentManager.h"
#include "compartment/fluid/SEGasCompartment.h"
#include "patient/SEPatient.h"
#include "patient/assessments/SEPulmonaryFunctionTest.h"
#include "patient/actions/SEHemorrhage.h"
#include "patient/actions/SESubstanceCompoundInfusion.h"
#include "patient/actions/SESubstanceInfusion.h"
#include "system/physiology/SEBloodChemistrySystem.h"
#include "system/physiology/SECardiovascularSystem.h"
#include "system/physiology/SEEnergySystem.h"
#include "system/physiology/SERespiratorySystem.h"
#include "substance/SESubstanceManager.h"
#include "substance/SESubstance.h"
#include "substance/SESubstanceCompound.h"
#include "properties/SEScalar0To1.h"
#include "properties/SEScalarFrequency.h"
#include "properties/SEScalarMassPerVolume.h"
#include "properties/SEScalarPressure.h"
#include "properties/SEScalarTemperature.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarVolume.h"
#include "properties/SEScalarVolumePerTime.h"
#include "properties/SEFunctionVolumeVsTime.h"
#include "properties/SEScalarMass.h"
#include "properties/SEScalarLength.h"

using namespace std;
using namespace libconfig;
using namespace std::chrono;

class SimMonitor : public PatientOut {

  private:
      CLA::Environment m_env;
      CLA::Monitor* m_monitor;
      double systolic_pressure;
      double diastolic_pressure;
      double heart_rate;
      double resp_rate;
      double oxygen_sat;
      std::shared_ptr<CLA::LOGGER> m_logger;
      std::map<std::string, CLA::Pump*> m_pumps;
      void Clear();
      /* chrono */
      system_clock::time_point m_start;
      system_clock::time_point m_engine_advance_end;
      system_clock::time_point m_command_start;
      system_clock::time_point m_command_end;
      system_clock::time_point m_infusion_start;
      system_clock::time_point m_infusion_end;
      time_t m_delta;
      time_t m_next_update_time;
      time_t m_engine_duration_ms;
      time_t m_command_duration_ms;
      time_t m_infusion_duration_ms;
      time_t m_next_update_time_duration_ms;
      time_t m_total_duration_ms;

  public:
      SimMonitor(std::shared_ptr<CLA::LOGGER>&, CLA::Environment&, \
                  std::map<std::string,CLA::Pump*>&);
      ~SimMonitor();
      void update(std::unique_ptr<PhysiologyEngine>&);
};

#endif
