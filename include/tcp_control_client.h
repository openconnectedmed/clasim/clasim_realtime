#ifndef TCP_CONTROL_CLIENT_H
#define TCP_CONTROL_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h> /* socket functions */
#include <sys/time.h>  /* FD_SET, FD_ISSET, FD_ZERO, timeval, fd_set */
#include <sys/select.h>/* select() */
#include <stdio.h>
#include <stdlib.h> /* exit, EXIT_FAILURE */
#include <string.h> /* memcpy */
#include <unistd.h>
#include <netdb.h> /* gethostbyname */
#include <netinet/in.h>/* sockaddr_in */
#include <iostream>
#include <memory>
#include "CLA_Logger.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson; /* third party library for parsing json data */
using namespace std;

namespace CLA {
  class tcp_control_client {
    /* impementation of a non-blocking tcp client to talk
    *  to the rosbridge udp server
    */
    private:
      struct hostent *hp; /* host information */
      struct sockaddr_in servaddr; /* server address information */
      struct timeval timeout;
      string json_message;
      string m_topic;
      string m_type;
      const char* m_host;
      int port;
      int fd, rec_value,recvlen;
      int SOCKET_TIMEOUT_SEC;
      int SOCKET_TIMEOUT_uSEC;
      socklen_t addrlen;
      Document document; /* to parse json data */
      Value m_patient_data;
      Value _topic;
      StringBuffer strbuf;
      fd_set set; /* descriptor read set*/
      std::shared_ptr<CLA::LOGGER> m_logger;
    public:
      tcp_control_client(std::shared_ptr<CLA::LOGGER>&, const string&, const string&);
      ~tcp_control_client();
      void setup_connection();
      void send_data_to_server(const string&);
      void publish_control_message(const string&);
      void waiting_to_send();
      void clear();
  };
}

#endif
