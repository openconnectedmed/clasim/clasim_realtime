#ifndef PatientOut_H
#define PatientOut_H

#include <chrono>
#include <libconfig.h++>
#include "CLA_Logger.h"
#include "Environment.h"
#include "Action.h"
/* Pulse header files */
#include "PulsePhysiologyEngine.h"

using namespace std::chrono;
using namespace libconfig;

class PatientOut {
  private:
    CLA::Environment m_env;
    std::shared_ptr<CLA::LOGGER> m_logger;
  public:
    CLA::Action m_action;
    duration<double> m_next_start_in;
    double advance_time;
    double secs;
    PatientOut(std::shared_ptr<CLA::LOGGER>&, CLA::Environment&, \
               std::map<std::string, CLA::Pump*>&);
    ~PatientOut();
    void LoadConfig(std::unique_ptr<PhysiologyEngine>&);
    virtual void update(std::unique_ptr<PhysiologyEngine>&) = 0;
};

#endif
