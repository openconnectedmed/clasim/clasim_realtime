#ifndef TCP_PUMP_CLIENT_H
#define TCP_PUMP_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h> /* socket functions */
#include <sys/time.h>  /* FD_SET, FD_ISSET, FD_ZERO, timeval, fd_set */
#include <sys/select.h>/* select() */
#include <stdio.h>
#include <stdlib.h> /* exit, EXIT_FAILURE */
#include <string.h> /* memcpy */
#include <unistd.h>
#include <netdb.h> /* gethostbyname */
#include <netinet/in.h>/* sockaddr_in */
#include <iostream>
#include <memory>
#include "CLA_Logger.h"
#include "rapidjson/document.h"

using namespace rapidjson; /* third party library for parsing json data */
using namespace std;

namespace CLA {
  class tcp_pump_client {
    /* impementation of a non-blocking tcp client to talk
    *  to the rosbridge udp server
    */
    private:
      struct hostent *hp; /* host information */
      struct sockaddr_in servaddr; /* server address information */
      struct timeval timeout;
      string json_message;
      double m_current_rate;
      double m_substance_last_sent_rate;
      double m_new_rate;
      double m_last_sent_rate;
      string m_topic;
      string m_type;
      string m_substance;
      static const int BUFF_SIZE = 100;
      char buff[BUFF_SIZE];
      const char* m_host;
      int port;
      int fd, rec_value,recvlen;
      int SOCKET_TIMEOUT_SEC;
      int SOCKET_TIMEOUT_uSEC;
      socklen_t addrlen;
      Document document; /* to parse json data */
      fd_set set; /* descriptor read set*/
      std::shared_ptr<CLA::LOGGER> m_logger;
    public:
      tcp_pump_client(const string&, const string&, const string&, \
        std::shared_ptr<CLA::LOGGER>&);
      ~tcp_pump_client();
      void setup_connection();
      double getCurrentInfusionRate(const string&);
      double get_new_rate(const string&);
      double waiting_for_new_rate(const string&);
      void clear();
  };
}

#endif
