
#ifndef CLALOGGER_H
#define CLALOGGER_H

#include <string>
#include <ctime> /* time_t, time, struct tm, localtime, strftime*/
#include <fstream>
#include <map>
#include <cstdarg>
#include <sys/time.h>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <cstring> /* strcat() */
#include <sys/time.h> /* timeval, gettimefday*/
#include <chrono>
#include "configure_clasim_rt_paths.h"

using namespace std;
using namespace std::chrono;

namespace CLA {
    class LOGGER {
	private:
	    string level_name;
	    const string m_filename;
      const string m_patient_name;
	    ofstream m_logstream[4];
	    ostringstream ss;
      bool m_verbosity = true;
	    enum LEVEL {DEBUG, INFO, WARN, ERROR, FATAL, NONE};
      LEVEL DEFAULT_LEVEL = LOGGER::DEBUG;
	    string current_time(bool);
	    string format_chrono_time(time_t);
	    string format_chrono_time(system_clock::time_point);
      string format_chrono_time_ms(system_clock::time_point);
	    string convert_level_to_string(LEVEL);
      string format_duration(time_t);
      map<std::string, LEVEL> logging_levels;
	public:
	    LOGGER(const string&); // filename
	    LOGGER(const string&, const string&); // filename, patient_name
      ~LOGGER();
	    void log_duration(system_clock::time_point, time_t, time_t, time_t, \
                        time_t, time_t);
      void log_duration(system_clock::time_point, time_t, time_t, time_t, \
                        time_t);
      void log_data(double, double, double);
      void setLevel(const string&);
      void setVerbosity(bool);
      template <typename T>
      void debug(const T& t) {
        if (LOGGER::DEBUG >= DEFAULT_LEVEL) {
          ss<< current_time(true);
          ss<< setw(2);
          ss<< " ";
          ss<< convert_level_to_string(LOGGER::DEBUG);
          ss<< setw(2);
          ss<< " ";
          ss<< t;
          m_logstream[0]<< ss.str() << endl;
          if (m_verbosity)
            cout << ss.str() <<endl;
          ss.str("");
        }
      }

      template <typename T>
      void info(const T& t) {
        if (LOGGER::INFO >= DEFAULT_LEVEL) {
          ss<< current_time(true);
          ss<< setw(2);
          ss<< " ";
          ss<< convert_level_to_string(LOGGER::INFO);
          ss<< setw(2);
          ss<< " ";
          ss<< " ";
          ss<< t;
          m_logstream[0]<< ss.str() << endl;
          if (m_verbosity)
            cout << ss.str() << endl;
          ss.str("");
        }
      }

      template <typename T>
      void warn(const T& t) {
        if (LOGGER::WARN >= DEFAULT_LEVEL) {
          ss<< current_time(true);
          ss<< setw(2);
          ss<< " ";
          ss<< convert_level_to_string(LOGGER::WARN);
          ss<< setw(2);
          ss<< " ";
          ss<< " ";
          ss<< t;
          m_logstream[0]<< ss.str() << endl;
          if (m_verbosity)
            cout << ss.str() << endl;
          ss.str("");
        }
      }
      template <typename T>
      void error(const T& t) {
        if (LOGGER::ERROR >= DEFAULT_LEVEL) {
          ss<< current_time(true);
          ss<< setw(2);
          ss<< " ";
          ss<< convert_level_to_string(LOGGER::ERROR);
          ss<< setw(2);
          ss<< " ";
          ss<< t;
          m_logstream[0]<< ss.str() << endl;
          if (m_verbosity)
            cout << ss.str() << endl;
          ss.str("");
        }
      }
      template <typename T>
      void fatal(const T& t) {
        if (LOGGER::FATAL >= DEFAULT_LEVEL) {
          ss<< current_time(true);
          ss<< setw(2);
          ss<< " ";
          ss<< convert_level_to_string(LOGGER::FATAL);
          ss<< setw(2);
          ss<< " ";
          ss<< t;
          m_logstream[0]<< ss.str() << endl;
          if (m_verbosity)
            cout << ss.str() << endl;
          ss.str("");
        }
      }
    };
}

#endif
