#ifndef Monitor_H_
#define Monitor_H_

#include "tcp_monitor_client.h"

namespace CLA {
  class Monitor {
    /* wrapper for monitor tcp client */
    private:
      CLA::tcp_monitor_client m_tcp_monitor_client;
      std::shared_ptr<CLA::LOGGER> m_logger;
    public:
      Monitor( std::shared_ptr<CLA::LOGGER>&);
      Monitor( std::shared_ptr<CLA::LOGGER>&, const string&, const string&);
      ~Monitor();
      void publish_patient_data(const string&);
  };
}

#endif
