#ifndef TIMER_H
#define TIME_H

#include <chrono>
#include <iostream>
#include <string>

using namespace std::chrono;

namespace CLA {
    class Timer {
        private:
            steady_clock::time_point start_timer;
            steady_clock::time_point end_timer;
            time_t duration; 

        public:
            Timer();
            ~Timer();
    };
}

#endif
