#ifndef ACTION_H
#define ACTION_H

#include <iostream>
#include <string>
#include <map>
#include "CLA_Logger.h"
#include "Pump.h"
#include "Environment.h"

/* pulse stuff */
#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"


#include "engine/SEDataRequestManager.h"
#include "engine/SEEngineTracker.h"
#include "engine/SEEventManager.h"
#include "compartment/SECompartmentManager.h"
#include "compartment/fluid/SEGasCompartment.h"
#include "patient/SEPatient.h"
#include "patient/assessments/SEPulmonaryFunctionTest.h"
#include "patient/actions/SEHemorrhage.h"
#include "patient/actions/SESubstanceCompoundInfusion.h"
#include "patient/actions/SESubstanceInfusion.h"
#include "system/physiology/SEBloodChemistrySystem.h"
#include "system/physiology/SECardiovascularSystem.h"
#include "system/physiology/SEEnergySystem.h"
#include "system/physiology/SERespiratorySystem.h"
#include "substance/SESubstanceManager.h"
#include "substance/SESubstance.h"
#include "substance/SESubstanceCompound.h"
#include "properties/SEScalar0To1.h"
#include "properties/SEScalarFrequency.h"
#include "properties/SEScalarMassPerVolume.h"
#include "properties/SEScalarPressure.h"
#include "properties/SEScalarTemperature.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarVolume.h"
#include "properties/SEScalarVolumePerTime.h"
#include "properties/SEFunctionVolumeVsTime.h"
#include "properties/SEScalarMass.h"
#include "properties/SEScalarLength.h"

using namespace std;

namespace CLA {
  class Action {
    private:
      SESubstance* m_bpdrug;
      SESubstanceInfusion* m_bpdrug_infusion;
      SESubstanceCompoundInfusion* m_infusion;
      SESubstanceCompound* m_saline;
      std::map<std::string,CLA::Pump*>& m_pumps;
      double bpdrug_prev_rate; /* cache the last received rate */
      double saline_prev_rate; /* cache the last received rate */
      double NorepiConcentration;
      double SalineBagVolume;
      double bpdrug_rate;
      double saline_rate;
      std::shared_ptr<CLA::LOGGER> m_logger;
      CLA::Environment m_env;
    public:
      Action(std::shared_ptr<CLA::LOGGER>&, std::map<std::string,CLA::Pump*>&);
      ~Action();
      void infuse_drug(std::unique_ptr<PhysiologyEngine>&);
      void initialize(std::unique_ptr<PhysiologyEngine>&);
      double get_bpdrug_rate() const;
      double get_saline_rate() const;
      void clear();
  };
}

#endif
