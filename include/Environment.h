
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

namespace CLA {
		struct Environment {
			double engine_timestep; // engine period
			double pulse_advance_time;
			double simulation_time; // grab from config file in seconds
			double simulation_realtime;
			unsigned long simulation_timesteps; // how long should simulation run
			unsigned long time_index; // track engine advancement time
			double simulation_injury_start;
			double simulation_injury_stop;
			double sim_currentTime;
			double bpdrug_rate;
			double saline_rate;
			unsigned long simulation_injury_start_timestep;
			unsigned long simulation_injury_stop_timestep;


		};
}

#endif
