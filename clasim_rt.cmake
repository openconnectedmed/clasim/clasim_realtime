cmake_minimum_required(VERSION 3.9.4)

project(clasim_rt)



#------------------------------------------------------------------------------------
# The location of libconfig.h++ and libconfig++.a
find_path(CONFIG++_INCLUDE_DIR libconfig.h++ /usr/include/)
find_library(CONFIG++_LIBRARY NAMES libconfig++.a PATH /usr/lib/)

#-----------------------------------------------------------------------------------

find_library(LOG4CPP_LIBRARY NAMES liblog4cpp.a PATH /usr/lib/)
if (LOG4CPP_LIBRARY)
    set(log4cpp_lib "${LOG4CPP_LIBRARY}")
else()
    set(log4cpp_lib)
endif()
#----------------------------------------------------------------------------------

#prosim depends on pulse this will call will give you access to pulse directories
#needed by prosim
find_package(Pulse REQUIRED NO_MODULE)

set(CLASIM_RT_CONFIG_DIR ${CMAKE_CURRENT_SOURCE_DIR}/resources/)
set(CLASIM_RT_RESULTS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/results/)
set(CLASIM_RT_LOGS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/logs/)
set(PULSE_STATE_DIR ${Pulse_INSTALL}/bin/states/)

file(MAKE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/results)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/logs)

configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/include/configure_clasim_rt_paths.h.in
    ${CMAKE_CURRENT_SOURCE_DIR}/include/configure_clasim_rt_paths.h
)
set( my_srcs
     CLA_Logger.cpp
     SimulationEngine.cpp
     Timer.cpp
     Action.cpp
     tcp_pump_client.cpp
     Pump.cpp
     tcp_monitor_client.cpp
     tcp_control_client.cpp
     Control.cpp
     Monitor.cpp
     SimMonitor.cpp
     PatientOut.cpp
)

add_executable(clasim_rt ${my_srcs})

#include directories
target_include_directories(clasim_rt PRIVATE
				${CMAKE_CURRENT_SOURCE_DIR}/include
				${CMAKE_CURRENT_SOURCE_DIR}/3rdParty/include
				${Pulse_INCLUDE_DIRS}
				${CONFIG++_INCLUDE_DIR})

#libraries
target_link_libraries(clasim_rt debug "${Pulse_DEBUG_LIBS}")
target_link_libraries(clasim_rt optimized
                      "${Pulse_RELEASE_LIBS}"
                      ${CONFIG++_LIBRARY}
                      ${log4cpp_lib}
                      )

install(TARGETS clasim_rt DESTINATION ${Pulse_INSTALL}/bin)
