#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <libconfig.h++>

#include "SimMonitor.h"
#include "CLA_Logger.h"
#include "SimulationEngine.h"
#include "Timer.h"
#include "Pump.h"
#include "Control.h"

#include "configure_clasim_rt_paths.h"

#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"

//types Pulse types will be using
#include "engine/SEDataRequestManager.h"
#include "engine/SEEngineTracker.h"
#include "engine/SEEventManager.h"
#include "compartment/SECompartmentManager.h"
#include "compartment/fluid/SEGasCompartment.h"
#include "patient/SEPatient.h"
#include "patient/assessments/SEPulmonaryFunctionTest.h"
#include "patient/actions/SEHemorrhage.h"
#include "patient/actions/SESubstanceCompoundInfusion.h"
#include "patient/actions/SESubstanceInfusion.h"
#include "system/physiology/SEBloodChemistrySystem.h"
#include "system/physiology/SECardiovascularSystem.h"
#include "system/physiology/SEEnergySystem.h"
#include "system/physiology/SERespiratorySystem.h"
#include "substance/SESubstanceManager.h"
#include "substance/SESubstance.h"
#include "substance/SESubstanceCompound.h"
#include "properties/SEScalar0To1.h"
#include "properties/SEScalarFrequency.h"
#include "properties/SEScalarMassPerVolume.h"
#include "properties/SEScalarPressure.h"
#include "properties/SEScalarTemperature.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarVolume.h"
#include "properties/SEScalarVolumePerTime.h"
#include "properties/SEFunctionVolumeVsTime.h"
#include "properties/SEScalarMass.h"
#include "properties/SEScalarLength.h"

string current_time(bool display_timezone);

using namespace libconfig;

int main() {
    CLA::Timer timer; /* time how long the simulation takes to run*/
    auto cf = std::make_shared<Config>();
    string f_path = prosim_config_dir+"scenario.cfg";
    const char* filepath = f_path.c_str();
    
    
    
    /* 
     * Can simulate a single patient as shown below, or multiple patients sequentially
     * by entering patients{"Patient 1 Name", "Patient 2 Name", "Patient 3 Name" }.
     * Other patient options are "Hassan" and "ExtremeFemale"
     */
    std::vector<std::string> patients{"Gus"};

    try {
      cf->readFile(filepath);
    }
    catch(const FileIOException &fioex) {
      std::cerr << "I/O error while reading file: "<< filepath <<std::endl;
    }
    catch(const ParseException &pex) {
      std::cerr <<"Parse error at "<< pex.getFile()<< ":"<< pex.getLine();
      std::cerr <<"-"<< pex.getError() << std::endl;
    }

    for (auto patient: patients) {
      Simulation(patient, cf);
    }
}

void Simulation(const string patient_name, const std::shared_ptr<Config>& cfg)
{
    // Create the engine and load the patient
    std::unique_ptr<PhysiologyEngine> pe = CreatePulseEngine(prosim_logs_dir+"SimulationEngine_"+patient_name+current_time(true)+".log");
    pe->GetLogger()->Info("CLA_Prosim Simulation");

    SEScalarTime startTime;
  // You can optionally specify a specific simulation time for the engine to use as its initial simulation time
  // If no time is provided, the simulation time that is in the state file will be used
  // Note the provided state files are named to include what is simulation time is
  	startTime.SetValue(0, TimeUnit::s);

    if (!pe->SerializeFromFile(pulse_state_dir+patient_name+"@0s.json", JSON, &startTime, nullptr))
	  {
	    pe->GetLogger()->Error("Could not load state, check the error");
	    return;
	  }
    // Create data requests for each value that should be written to the output log as the engine is executing
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("SystolicArterialPressure", PressureUnit::mmHg);
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("DiastolicArterialPressure", PressureUnit::mmHg);
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("MeanArterialPressure", PressureUnit::mmHg);
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("HeartRate", FrequencyUnit::Per_min);
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("RespirationRate", FrequencyUnit::Per_min);
    pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest("OxygenSaturation");
    pe->GetEngineTracker()->GetDataRequestManager().SetResultsFilename(prosim_results_dir+"PulseSimEngine_"+patient_name+current_time(true)+".txt");

    SEHemorrhage hemorrhageLeg;
    double initialMAP = 70.0;
    double currentMAP;
    double hemorrhage_rate =125;
    bool STOP_HEMORRHAGE = false;
    bool START_DEVICES = false;
    CLA::Environment sim_env;
    //std::shared_ptr<CLA::LOGGER> logger = std::make_shared<CLA::LOGGER>(patient_name,patient_name);

    // add timestamp to filenames
    std::shared_ptr<CLA::LOGGER> logger = std::make_shared<CLA::LOGGER>(patient_name);

    //logger->setLevel("INFO");/*DEFAULT: DEBUG */
    Global_LoadConfig(logger, pe, cfg, sim_env);
    CLA::Control control(logger, "/control", "std_msgs/String");

    std::map<std::string, CLA::Pump*> my_pumps; /* pumps */
    /* Params: drug name, pump topic to subscribe to, type of data published on that topic*/
    CLA::Pump bpdrug_pump(logger, "Norepinephrine", "/bpdrug_pumpout", "std_msgs/Float64");
    CLA::Pump saline_pump(logger, "Saline", "/saline_pumpout", "std_msgs/Float64");
    my_pumps["bpdrug"] = &bpdrug_pump;
    my_pumps["saline"] = &saline_pump;

    /* simulated monitor */
    SimMonitor sim_monitor(logger, sim_env, my_pumps);
    sim_monitor.LoadConfig(pe);

    // stop when the set time is reached
    while (sim_env.time_index <= sim_env.simulation_timesteps ) {
        currentMAP = pe->GetCardiovascularSystem()->GetMeanArterialPressure(PressureUnit::mmHg);
        // event_hemorrage_start
        if(sim_env.time_index == sim_env.simulation_injury_start_timestep) {
          // Hemorrhage Starts - instantiate a hemorrhage action and have the engine process it
          logger->info("Hemorrhage Started");
          logger->info("Patient hemorrhaging at a rate of "+ \
                       to_string((int)hemorrhage_rate)+ " mL/min");
          hemorrhageLeg.SetCompartment(pulse::VascularCompartment::RightLeg);
          //the rate of hemorrhage
          hemorrhageLeg.GetRate().SetValue(hemorrhage_rate,VolumePerTimeUnit::mL_Per_min);
          pe->ProcessAction(hemorrhageLeg);
          STOP_HEMORRHAGE = true;
        }
        if( currentMAP <= initialMAP && STOP_HEMORRHAGE) {
          logger->info("Hemorrhage stopped");
          hemorrhageLeg.SetCompartment(pulse::VascularCompartment::RightLeg);
          hemorrhageLeg.GetRate().SetValue(0,VolumePerTimeUnit::mL_Per_min);
          pe->ProcessAction(hemorrhageLeg);
          STOP_HEMORRHAGE = false;
          START_DEVICES = true; /* Begin real time simulation */
          /* update environment variables and switch to realtime */
          sim_env.simulation_time = sim_env.simulation_realtime;
          sim_env.simulation_timesteps =  sim_env.time_index + \
                            sim_env.simulation_time/sim_env.pulse_advance_time;
          logger->info("Realtime simulation started");
          logger->info("Devices started");
          /* start executing algorithm */
          control.publish_control_message("start");
        }
        /* We both track data and advance engine inside the update method */
        if (START_DEVICES) {
          sim_monitor.update(pe);
        }
        /* This stops running once realtime starts */
        if (!START_DEVICES) {
          pe->GetEngineTracker()->TrackData(pe->GetSimulationTime(TimeUnit::s));
          pe->AdvanceModelTime();
        }
        sim_env.time_index++;
    }// End while looop

    /* stop executing algorithm */
    control.publish_control_message("stop");

}// End Simulation function

void Global_LoadConfig(std::shared_ptr<CLA::LOGGER>& logger, std::unique_ptr<PhysiologyEngine>&
engine, const std::shared_ptr<Config>& cf, CLA::Environment& env) {
    try {
      env.simulation_realtime = cf->lookup("simulation.time.run");
    }
    catch(const SettingNotFoundException) {
      logger->error("Setting Not Found: simulation.time.run");
      exit(EXIT_FAILURE);
    }
    try {
      env.simulation_time = cf->lookup("simulation.time.simulation_time_run");
    }
    catch(const SettingNotFoundException) {
      logger->error("Setting Not Found: simulation.time.simulation_time_run");
      exit(EXIT_FAILURE);
    }
    /* get the injury start time */
    try {
      env.simulation_injury_start = cf->lookup("simulation.time.injury_start");
    }
    catch(const SettingNotFoundException) {
      logger->error("Setting Not Found: simulation.time.injury_start");
      exit(EXIT_FAILURE);
    }
    /* get the injury stop time */
    try {
      env.simulation_injury_stop = cf->lookup("simulation.time.injury_stop");
    }
    catch(const SettingNotFoundException) {
      logger->error("Setting Not Found: simulation.time.injury_stop");
      exit(EXIT_FAILURE);
    }
    try {
      env.pulse_advance_time = cf->lookup("pulse.advance_time");
    }
    catch(const SettingNotFoundException) {
      logger->error("Setting Not Found: pulse.advance_time");
      exit(EXIT_FAILURE);
    }
    env.time_index = 0; // initialize the time index
    env.engine_timestep = engine->GetTimeStep(TimeUnit::s);
    env.simulation_timesteps = env.simulation_time/env.engine_timestep;
    env.simulation_injury_start_timestep = env.simulation_injury_start/env.engine_timestep;
    env.simulation_injury_stop_timestep = env.simulation_injury_stop/env.engine_timestep;
}// End Global_LoadConfig

string current_time(bool display_timezone) {
    struct timeval tv; /* for seconds and micro seconds */
    gettimeofday(&tv , NULL); /* populate `tv` with time since epoch */
    char buffer[50];
    char m_sec_buff[10];
    char tz_buff[10];
    /* this is where the milli seconds come from :) */
    uint m_secs = static_cast<uint>(tv.tv_usec)/1000;
    sprintf(m_sec_buff, "%.3u", m_secs); /*cast unit to string */

    if (display_timezone) {
      strftime(buffer, 50, "%Y-%m-%d-%H %M %S,", localtime(&tv.tv_sec));
      strftime(tz_buff, 10, "  %Z", localtime(&tv.tv_sec));
      strcat(buffer, m_sec_buff);
      strcat(buffer, tz_buff);
    }
    else {
      strftime(buffer, 50, "%Y-%m-%d-%H %M %S", localtime(&tv.tv_sec));
    }
    cout << "time for files" << buffer <<endl;
    return buffer;
}
