#include "Control.h"

using namespace CLA;

/* wrapper for tcp client */
Control::Control(std::shared_ptr<CLA::LOGGER>& logger, const string& topic, const
string& type) : m_logger(logger), m_tcp_control_client(logger, topic, type) {
    m_tcp_control_client.setup_connection(); /* connection setup to server via tcp socket */
}

void Control::publish_control_message (const string& data) {
    m_tcp_control_client.publish_control_message(data);
}

Control::~Control() {
}
