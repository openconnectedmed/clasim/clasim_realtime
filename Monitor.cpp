#include "Monitor.h"

using namespace CLA;

/* wrapper for tcp client */
Monitor::Monitor(std::shared_ptr<CLA::LOGGER>& logger, const string& topic, const
string& type) : m_logger(logger), m_tcp_monitor_client(logger, topic, type) {
    m_tcp_monitor_client.setup_connection(); /* connection setup to server via tcp socket */
}

void Monitor::publish_patient_data (const string& data) {
    m_tcp_monitor_client.publish_patient_data(data);
}

Monitor::~Monitor() {
}
