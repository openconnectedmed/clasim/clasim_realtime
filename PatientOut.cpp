#include "PatientOut.h"

PatientOut::PatientOut(std::shared_ptr<CLA::LOGGER>& logger, CLA::Environment& \
env, std::map<std::string,CLA::Pump*>& pumps) : m_logger(logger), m_env(env),
m_action(logger, pumps) {
}

void PatientOut::LoadConfig(std::unique_ptr<PhysiologyEngine>& engine) {
    /* pulse engine advance time */
    advance_time = m_env.pulse_advance_time;

    /* run update method at rate of `secs` in realtime*/
    secs = advance_time;
    
    /* convert to duration seconds */
    m_next_start_in = duration<double>(secs);
    
    /*intialize action -> prepare for infusions */
    m_action.initialize(engine);
}

PatientOut::~PatientOut() {
    m_logger->info("End of simulation");
}
